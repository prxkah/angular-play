import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';

interface State {
  todos: any[];
  todo: any;
  message: string;
}

const initialState: State = {
  todos: [],
  todo: null,
  message: 'hello'
};

@Injectable({ providedIn: 'root' })
export class TodosService {
  // state
  private readonly _state = new BehaviorSubject<State>(initialState);
  private readonly state$ = this._state.asObservable();
  private get state() {
    return this._state.getValue();
  }

  // selectors
  selectTodos = () => this.selectState(state => state.todos);
  selectTodo = () => this.selectState(state => state.todo);

  // actions
  add = (title: string) => {
    const id = new Date().getTime();
    const todo = { id, title };
    this.setState({ todos: [...this.state.todos, todo] });
  };

  remove = (id: number) => {
    const todos = this.state.todos.filter(item => item.id !== id);
    const todo = this.state.todo
      ? this.state.todo.id === id
        ? null
        : this.state.todo
      : null;

    this.setState({ todos, todo });
  };

  setActive(id: number) {
    const todo = this.state.todos.find(item => item.id === id);
    this.setState({ todo });
  }

  // helpers
  private setState = (newState: Partial<State>) =>
    this._state.next({ ...this.state, ...newState });

  private selectState = <T>(project: (value: State) => T) =>
    this.state$.pipe(map(project));
}

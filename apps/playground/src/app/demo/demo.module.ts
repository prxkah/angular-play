import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DemoComponent } from './demo.component';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { TodoApplicationComponent } from './containers/todo-application/todo-application.component';
import { TodoAddComponent } from './components/todo-add/todo-add.component';
import { TodoListComponent } from './components/todo-list/todo-list.component';
import { TodoComponent } from './components/todo/todo.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild([{ path: '', component: DemoComponent }])
  ],
  declarations: [
    DemoComponent,
    TodoApplicationComponent,
    TodoAddComponent,
    TodoListComponent,
    TodoComponent
  ]
})
export class DemoModule {}

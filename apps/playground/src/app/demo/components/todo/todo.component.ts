import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-todo',
  template: `
    <div *ngIf="todo">
      <h1>{{todo.title}}</h1>
      <h6>Id: {{todo.id}}</h6>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TodoComponent {
  @Input() todo: any;
}

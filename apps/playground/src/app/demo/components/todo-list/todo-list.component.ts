import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-todo-list',
  template: `
    <table *ngIf="todos.length > 0; else empty" class="table table-striped mt-4">
      <thead>
        <tr>
          <td>Id</td>
          <td>Title</td>
          <td></td>
        </tr>
      </thead>
      <tbody>
        <tr *ngFor="let todo of todos">
          <td>{{todo.id}}</td>
          <td>{{todo.title}}</td>
          <td class="text-right">
            <button (click)="view.emit(todo.id)" class="btn btn-primary btn-sm mr-2">View</button>
            <button (click)="remove.emit(todo.id)" class="btn btn-danger btn-sm">Remove</button>
          </td>
        </tr>
      </tbody>
    </table>

    <ng-template #empty>
      <div class="alert alert-info mt-3">
        <p>Nothing to show...</p>
      </div>
    </ng-template>
  `,
  styles: []
})
export class TodoListComponent {
  @Output() remove = new EventEmitter();
  @Output() view = new EventEmitter();
  @Input() todos: any[] = [];
}

import { Component, EventEmitter, Output } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-todo-add',
  template: `
  <div class="form-group">
    <label>Title</label>
    <input [formControl]="title" class="form-control"/>
  </div>
  <button (click)="add()" class="btn btn-primary">Add</button>
  `
})
export class TodoAddComponent {
  @Output() added = new EventEmitter();
  title = new FormControl();

  add() {
    this.added.emit(this.title.value);
    this.title.setValue(null);
  }
}

import { Component } from '@angular/core';
import { TodosService } from '../../state/todos.service';

@Component({
  selector: 'app-todo-application',
  template: `
    <app-todo-add (added)="addTodo($event)"></app-todo-add>
    <app-todo-list [todos]="todos$ | async" (remove)="removeTodo($event)" (view)="viewTodo($event)"></app-todo-list>
    <app-todo [todo]="todo$ | async"></app-todo>
  `
})
export class TodoApplicationComponent {
  todo$ = this.todosService.selectTodo();
  todos$ = this.todosService.selectTodos();

  constructor(private todosService: TodosService) {}

  addTodo(title: string) {
    this.todosService.add(title);
  }

  removeTodo(id: number) {
    this.todosService.remove(id);
  }

  viewTodo(id: number) {
    this.todosService.setActive(id);
  }
}

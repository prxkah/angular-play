import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-demo',
  template: `
    <div class="col-md-6">
      <app-todo-application></app-todo-application>
    </div>
  `,
  styles: []
})
export class DemoComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}

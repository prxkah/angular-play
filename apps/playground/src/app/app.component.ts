import { Component } from '@angular/core';

@Component({
  selector: 'angular-play-root',
  templateUrl: './app.component.html'
})
export class AppComponent {}

import { Component, OnInit, Type } from '@angular/core';
import { ModalRef } from './modal-ref';

@Component({
  selector: 'app-modal',
  template: `
    <ng-container *ngComponentOutlet="componentType"></ng-container>
  `
})
export class ModalComponent implements OnInit {
  componentType: Type<any>;

  constructor(private modalRef: ModalRef) {}

  ngOnInit() {
    this.componentType = this.modalRef.componentType;
  }
}

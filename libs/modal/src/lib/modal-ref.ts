import { Injectable, Type } from '@angular/core';
import { OverlayRef } from '@angular/cdk/overlay';
import { Subject } from 'rxjs';

export interface ModalCloseEvent {
  type: string
}

@Injectable()
export class ModalRef<T = any> {
  private closed = new Subject<ModalCloseEvent>();
  closed$ = this.closed.asObservable();

  constructor(public overlayRef: OverlayRef, public componentType: Type<any>, public data: any) {}

  close(type: string) {
    this.overlayRef.detach();
    this.closed.next({ type });
    this.closed.complete();
  }
}

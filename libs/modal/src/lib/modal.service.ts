import { Injectable, Injector, Type } from '@angular/core';
import { Overlay } from '@angular/cdk/overlay';
import { ComponentPortal, PortalInjector } from '@angular/cdk/portal';
import { ModalRef } from './modal-ref';
import { ModalComponent } from './modal.component';

@Injectable()
export class ModalService {
  constructor(private overlay: Overlay, private injector: Injector) {}

  open(componentType: Type<any>, data: any) {
    const overlayRef = this.createOverlay();

    const modalRef = new ModalRef(overlayRef, componentType, data);

    const injector = this.createInjector(this.injector, modalRef);

    overlayRef.attach(new ComponentPortal(ModalComponent, null, injector));

    return modalRef;
  }

  createOverlay() {
    return this.overlay.create({
      hasBackdrop: true
    });
  }

  createInjector(injector: Injector, modalRef: ModalRef) {
    return new PortalInjector(injector, new WeakMap([[ModalRef, modalRef]]));
  }
}

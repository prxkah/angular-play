import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalService } from './modal.service';
import { OverlayModule } from '@angular/cdk/overlay';
import { ModalComponent } from './modal.component';

@NgModule({
  declarations: [ModalComponent],
  imports: [CommonModule, OverlayModule],
  providers: [ModalService],
  entryComponents: [ModalComponent]
})
export class ModalModule {}
